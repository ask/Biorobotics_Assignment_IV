% Working directory needs to be the same
% Run this declaration before using simulink
g = -9.81;
m = 1;      %mass
l_0 = 100;   %length of spring
k = 1;     %spring constant in N/cm <= 100N/m = 1N/cm
r_0 = 120;      %initial position

